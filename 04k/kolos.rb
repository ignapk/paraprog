start = Time.new(2023, 3, 24, 17, 0, 0)
duration = 2 * 60 * 60 + 20 * 60 + 0
minh = 10
maxh = 18

tmp = 0
if start.friday? and start.hour > maxh then
  tmp = 3
elsif start.saturday? then
  tmp = 2
elsif start.sunday? then
  tmp = 1
end

if tmp > 0 then
  start = Time.new(start.year, start.month, start.mday + tmp, minh, 0, 0)
end

if start.hour < minh then
  start = Time.new(start.year, start.month, start.mday, minh, 0, 0)
end

cutoff = Time.new(start.year, start.month, start.mday, maxh, 0, 0)
diff = cutoff - start

if duration > diff then
  if start.friday? then
    result = Time.new(start.year, start.month, start.mday + 3, minh, 0, 0) + (duration - diff)
  else
    result = Time.new(start.year, start.month, start.mday + 1, minh, 0, 0) + (duration - diff)
  end
else
  result = start + duration;
end

puts "#{result.to_s}"
