def z04(a, b, c)
  d = b*b - 4*a*c
  if d > 0 then
    x1 = (-b + Math.sqrt(d))/(2*a)
    x2 = (-b - Math.sqrt(d))/(2*a)
    puts "#{x1} #{x2}"
  elsif d == 0 then
    x = (-b)/(2*a)
    puts "#{x}"
  else
    puts "none"
  end
end

def z04_2wersja(a, b, c)
  d = b*b - 4*a*c
  puts (-b + Math.sqrt(d))/(2*a), (-b - Math.sqrt(d))/(2*a) if d >= 0
  puts "none" if d < 0
end

def z05(n)
  ret = 1;
  for i in 2..n
    ret *= i;
  end
  puts ret
end

class Osoba
  def initialize(imie, nazwisko)
    @imie = imie
    @nazwisko = nazwisko
  end

  def imie()
    puts @imie
  end

  def nazwisko()
    puts @nazwisko
  end
  
  def to_s()
    puts @imie + " " + @nazwisko
  end
end

def z06()
  ignacy = Osoba.new("Ignacy", "Kuchcinski")
  krystian = Osoba.new("Krystian", "Kogut")
  ignacy.imie()
  ignacy.nazwisko()
  ignacy.to_s()
  krystian.imie()
  krystian.nazwisko()
  krystian.to_s()
end



a = 1.0
b = 2.0
c = 1.0
z04(a,b,c)
z05(100000)
z06()
