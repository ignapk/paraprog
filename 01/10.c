#include <stdio.h>

int power (int x, int n)
{
	if (n == 0)
		return 1;
	if (n%2 == 1)
		return x * power (x, n - 1);
	return power (x, n / 2) * power (x, n / 2);
}

int main ()
{
	int x, n;
	scanf ("%d%d", &x, &n);
	printf ("%d\n", power (x, n));
}
