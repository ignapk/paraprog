#include <stdio.h>

int s (int n, int k)
{
	if (n <= 0)
		return 1;
	return (n * s (n-k, k));
}

int main ()
{
	int n;
	scanf ("%d", &n);
	printf ("%d\n", s(n,1));
	printf ("%d\n", s(n,2));
	return 0;
}
