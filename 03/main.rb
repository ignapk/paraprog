class Hash
  def wyswietl_ocene(nazwa)
    puts "#{self[nazwa]}"
  end

  def oblicz_srednia()
    sum = 0.0
    i = 0.0
    self.each do |nazwa,ocena|
      sum += ocena
      i+= 1.0
    end
    puts "#{sum/i}"
  end
end

def z03()
  dzienniczek = {}
  dzienniczek["io"] = 3
  dzienniczek["jipp"] = 5
  dzienniczek["pwjr"] = 5
  dzienniczek["wssi"] = 4
  dzienniczek.wyswietl_ocene("jipp")
  dzienniczek.oblicz_srednia()
end

def z04(a, b, c)
  d = b*b - 4*a*c
  if d > 0 then
    x1 = (-b + Math.sqrt(d))/(2*a)
    x2 = (-b - Math.sqrt(d))/(2*a)
    puts "#{x1} #{x2}"
  elsif d == 0 then
    x = (-b)/(2*a)
    puts "#{x}"
  else
    puts "none"
  end
end

def z04_2wersja(a, b, c)
  d = b*b - 4*a*c
  puts (-b + Math.sqrt(d))/(2*a), (-b - Math.sqrt(d))/(2*a) if d >= 0
  puts "none" if d < 0
end

def z05(n)
  ret = 1;
  for i in 2..n
    ret *= i;
  end
  puts ret
end

class Osoba
  def initialize(imie, nazwisko)
    @imie = imie
    @nazwisko = nazwisko
  end

  def imie()
    puts @imie
  end

  def nazwisko()
    puts @nazwisko
  end
  
  def to_s()
    puts @imie + " " + @nazwisko
  end
end

def z06()
  ignacy = Osoba.new("Ignacy", "Kuchcinski")
  krystian = Osoba.new("Krystian", "Kogut")
  ignacy.imie()
  ignacy.nazwisko()
  ignacy.to_s()
  krystian.imie()
  krystian.nazwisko()
  krystian.to_s()
end

class Hash
  def znajdz_dziadka(imie)
    self.each{|syn,ojciec| puts "#{ojciec}" if self[imie] = syn}
  end
end

def z07()
  drzewo = {}
  drzewo["Charles"]="Richard"
  drzewo["Richard"]="William"
  drzewo.znajdz_dziadka("Charles")
end



a = 1.0
b = 2.0
c = 1.0
z03()
z04(a,b,c)
z05(100000)
z06()
z07()
