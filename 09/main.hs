main :: IO()
main = do
     print(zad1([1,2,3,4]))

zad1 :: [Integer] -> [Integer]
zad1 [x] = [x]
zad1 a = [last a] ++ (zad1(init a))
